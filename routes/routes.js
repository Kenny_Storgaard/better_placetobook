'use strict';
const evController = require('../controllers/eventController')
const orgController = require('../controllers/organizerController')
const scripts = require('../public/js/scripts.js')
const express = require('express')
const router = express.Router();
//should this be in here?
const mongoose = require('mongoose');
const Organizer = mongoose.model('Organizer');

router.get('/', (req, res) => {
    // console.log(req.session)
    // const {
    //     userId
    // } = req.session
    res.render('loginTemplate');
})

router.route('/events')
    .get((request, response) => {
        response.render('dropDownEvents')
    })

    .post((req, res) => {

        evController.createEvent(req.body).then(() => {}).catch((error) => {
            res.send('Fejl i opret event')
        })

        res.render('eventsOverview')

    });

router.get('/home', (req, res) => {
    res.render('eventsOverview');
})

// Redirecter til login, hvis ikke bruger er authenticated
const redirectLogin = (req, res, next) => {
    if (!req.session.userId) {
        res.render('loginTemplate')
    } else {
        next()
    }
}
// Hvis bruger er logget ind redirectes til Home (oversigt over arrangementer)
const redirectHome = (req, res, next) => {
    if (req.session.userId) {
        res.render('eventsOverview')
    } else {
        next()
    }
}


// Express-Session Login
router.get('/login', (req, res) => {
    // req.session.userId = 
})

router.post('/login', redirectHome, (req, res) => {

    // Opret temp organizer
    const tempOrg = new Organizer({
        'email': req.body.email
    })
    tempOrg.setPassword(req.body.password)
    if (req.body.email && req.body.password) {
        // Tjek om bruger findes
        orgController.findOrganizer(tempOrg.email).then(exists => {
            if (exists !== null) {
                if (exists.validPassword(req.body.password)) {
                    req.session.userId = exists.id
                    console.log('Exists id: ' + exists.id)
                    console.log('Password er korrekt!')
                    return res.render('eventOverviewTemplate')
                }
            } else {
                console.log('Email eller password forkert')
                res.render('loginTemplate')
            }
        })
    } else {
        console.log('Email eller password ikke indtastet')
        res.render('loginTemplate')
    }
})

// Express-Session Opret
router.get('/opretbruger', (req, res) => {
    res.render('createOverview')
})

router.get('/toCreate', (request, response) => {
    response.render('createEventTemplate');
})

router.post('/opretbruger', redirectHome, (req, res) => {
    const tempOrg = req.body
    if (tempOrg.email && tempOrg.password) {

        orgController.findOrganizer(tempOrg.email).then(exists => {
            if (exists === null) {
                orgController.createOrganizer(tempOrg)
                console.log('Kunde oprettet!')
                return res.render('eventsOverview')
            } else {
                console.log('Kunde eksistere allerede!')
                res.render('createOverview')
            }
        })
        // req.session.userId = tempOrg.id
    }
})

// Express-Session Logout
router.post('/logout', redirectLogin, (req, res) => {
    req.session.destroy(err => {
        if (err) {
            return res.render('eventsOverview')
        }
        res.clearCookie(SESS_NAME)
        res.render('loginTemplate')
    })
})
router.get('/toOverview', (request, response) => {
    response.render('eventOverviewTemplate');
})
router.get('/toTickets', (request, response) => {
    response.render('eventAndTicketsTemplate');
})
module.exports = router