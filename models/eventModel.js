'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Nogle af atributterne skal flyttes til ticket schema
const eventSchema = new Schema({
    eventName: String,
    description: String,
    location: [],
    time: Date,
    salesPeriod_Start: Date,
    salesPeriod_End: Date,
    capacity: Number,
    tickets: {
        type: Schema.Types.ObjectId,
        ref: 'Ticket'
    },
    company: {
        type: Schema.Types.ObjectId,
        ref: 'Company'
    }
});

module.exports = mongoose.model('Event', eventSchema);