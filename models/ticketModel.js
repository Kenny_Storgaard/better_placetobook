'use strict';
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ticketSchema = new Schema({
    ticketType: String,
    customerFirstName: String,
    customerLastName: String,
    customerEmail: String,
    customertelephone: String,
    customerAddress: String,
    customerZipCode: String,
    customerCity: String,
    customerCountry: String,
    event: {
        type: Schema.Types.ObjectId,
        ref: 'Event'
    }
    //extraServices: null,
    //productPackage: String,
    //salesDesign: null,
    //ticketDesign: null, //feature? 
    //ticketContent: null,

});

module.exports = mongoose.model('Ticket', ticketSchema);