'use strict';
const mongoose = require('mongoose')
const crypto = require('crypto');
const Schema = mongoose.Schema

const organizerSchema = new Schema({
    //Company information
    companyName: String,
    cvrNumber: String,
    eanNumber: String,
    customerServiceEmail: String,
    customerServiceTelephone: String,
    companyTelephone: String,
    address: String,
    zipCode: String,
    Country: String,
    City: String,
    //responsible employee:
    responsibleFirstName: String,
    responsibleLastName: String,
    //company login:
    email: {
        type: String,
        unique: true,
        required: true
    },
    // password: {
    //     type: String,
    //     required: true
    // },
    hash: String,
    salt: String,
    //events organized by this company: 
    events: [{
        type: Schema.Types.ObjectId,
        ref: 'Event'
    }]
})

// Opret og valider password
organizerSchema.methods.setPassword = function (password) {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
};
organizerSchema.methods.validPassword = function (password) {
    var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
    return this.hash === hash;
};

module.exports = mongoose.model('Organizer', organizerSchema);