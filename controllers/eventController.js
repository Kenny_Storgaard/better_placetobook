'use strict';
const mongoose = require('mongoose');
const event = mongoose.model('Event');
const ticket = mongoose.model('Ticket');


exports.createEvent = function (eventInformation) {
  
  // const locationArray = [eventInformation[0], eventInformation[3], eventInformation[4], eventInformation[5],eventInformation[6]]
  const locationArray = [eventInformation.locationName, eventInformation.address, eventInformation.zipcode, eventInformation.city,eventInformation.country]
 
  const new_event = new event({
    "eventName": eventInformation.eventName,
    "description": eventInformation.description,
    "location": locationArray,
    "time": eventInformation.time,
    "salesPeriod_Start": eventInformation.start,
    "salesPeriod_End": eventInformation.end,
    "capacity": eventInformation.capacity,
    "tickets": eventInformation.tickets,
    "company": eventInformation.company
  });


  // const new_event = new event({
  //   "eventName": eventInformation[0],
  //   "description": eventInformation[1],
  //   "location": locationArray,
  //   "time": eventInformation[7],
  //   "salesPeriod_Start": eventInformation[8],
  //   "salesPeriod_End": eventInformation[9],
  //   "capacity": eventInformation[10],
  //   "tickets": eventInformation[11],
  //   "company": eventInformation[12]
  // });
  console.log('new Event: ' + new_event)
  return new_event.save()
};