'use strict';
const mongoose = require('mongoose');
const Organizer = mongoose.model('Organizer');

exports.createOrganizer = function (obj) {
    const new_org = new Organizer({
        'email': obj.email,
        'companyName': obj.companyName,
        'customerServiceEmail': obj.customerServiceEmail,
        'customerServiceTelephone': obj.customerServiceTelephone,
        'companyTelephone': obj.companyTelephone,
        'address': obj.address,
        'zipcode': obj.zipcode,
        'Country': obj.Country,
        'City': obj.City,
        'responsibleFirstName': obj.responsibleFirstName,
        'responsibleLastName': obj.responsibleLastName,
        'hash': '',
        'salt': '',
        // 'events': "",
        'cvrNumber': obj.cvrNumber,
        'eanNumber': obj.eanNumber
    })
    new_org.setPassword(obj.password)
    // console.log('New Obj: ' + new_org)
    return new_org.save()
}

exports.findOrganizer = function (email) {
    return Organizer.findOne({
        email: email
    }).exec()
};

// exports.createJoke = function (setup, punchline) {
//     const new_joke = new jokes({
//         setup: setup,
//         punchline: punchline
//     });
//     console.log(new_joke)
//     return new_joke.save()
// }