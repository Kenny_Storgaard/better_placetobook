//INITIALIZATION
const express = require('express');
const session = require('express-session')
const bodyParser = require('body-parser');
const pug = require('pug')
const config = require('./config'); //config fil i roden
const path = require('path');
const event = require('./models/eventModel.js')
const ticket = require('./models/ticketModel.js')
const organizer = require('./models/organizerModel.js')

//Express-Session config
const ONE_HOUR = 1000 * 60 * 60
const {
    NODE_ENV = 'development',
    SESS_NAME = 'sid',
    SESS_SECRET = 'hemmeligt_kodeord',
    SESS_LIFETIME = ONE_HOUR
} = process.env 

const IN_PROD = NODE_ENV === 'production'

//MONGODB & MONGOOSE SETUP
const mongoose = require('mongoose')
mongoose.Promise = global.Promise // kan lave promise i mongoose
mongoose.connect(config.mongoDBhost, {
    useNewUrlParser: true,
    useCreateIndex: true,
    autoIndex: false
});


//MIDDLEWARE
const app = express();
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(express.static('public'));
app.set('views', './views');
app.set('view engine', 'pug');
app.use(express.static(path.join(__dirname, 'public')));

//Express-Session config
app.use(session({
    name:SESS_NAME,
    resave: false,
    saveUninitialized: false,
    secret: SESS_SECRET,
    cookie: {
        maxAge: SESS_LIFETIME,
        sameSite: true,
        secure: IN_PROD
    }
}))


//ROUTES FOR THE APP
const ROUTES = require('./routes/routes')
app.use('/', ROUTES)

//START THE SERVER
app.listen(config.PORT, () => console.log(`Listening on port ${config.PORT}!`))